provider "aws" {
  region = var.region
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}

 terraform {
   backend "http" {
   }
 }

data "aws_eks_cluster" "cluster"  {
  name = module.devops5pasos-eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster"  {
  name = module.devops5pasos-eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.9"
}


resource "aws_eip" "nat" {
  count = 3
  vpc = true
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "k8s-training-vpc"
    cidr = "10.0.0.0/20"

  azs = ["us-east-1a", "us-east-1b", "us-east-1c"]
  public_subnets = ["10.0.2.0/24", "10.0.4.0/24", "10.0.6.0/24"]
  private_subnets  = ["10.0.12.0/24", "10.0.13.0/24", "10.0.14.0/24"]

  enable_nat_gateway = true
  single_nat_gateway  = false
  reuse_nat_ips       = true  
  external_nat_ip_ids = "${aws_eip.nat.*.id}"

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Terraform = "true"
    Environment = "dev"
    Owner = "Manu Barrios"
  }
}


module "devops5pasos-eks" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_name = "devops5pasos-eks"
  subnets      = flatten([module.vpc.public_subnets, module.vpc.private_subnets])
  vpc_id       = "${module.vpc.vpc_id}"
  cluster_version = "1.20"

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  worker_ami_name_filter                = "amazon-eks-node-1.20-v20210826"
  manage_worker_iam_resources           = true
  enable_irsa                           = true

 
  #manage_aws_auth = false
  worker_groups = [
    {
      instance_type = "m4.large"
      asg_max_size  = 3
      asg_min_size  = 1
      asg_desired_capacity = 2
      root_volume_type = "gp2"
      enable_monitoring     = false
      public_ip             = false
      tags = [{
        key                 = "Project"
        value               = "devops5pasos-iac"
        propagate_at_launch = true
      }]
    }
  ]


  tags = {
    Terraform = "true"
    Environment = "dev"
    Owner = "Manu Barrios"
  }
}
