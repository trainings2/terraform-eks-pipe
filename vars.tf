variable "region" {
  default = "us-east-1"
  description = "region"
}

variable "AWS_ACCESS_KEY_ID" {
  description = "access key AWS"
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "secret key AWS"
}

